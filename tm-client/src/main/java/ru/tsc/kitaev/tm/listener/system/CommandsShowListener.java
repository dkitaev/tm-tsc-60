package ru.tsc.kitaev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.listener.AbstractListener;

@Component
public final class CommandsShowListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String command() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list commands...";
    }

    @Override
    @EventListener(condition = "@commandsShowListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMMANDS]");
        for (@NotNull final AbstractListener listener : listeners) {
            @Nullable String commandName = listener.command();
            if (commandName != null || !commandName.isEmpty())
                System.out.println(commandName + ": " + listener.description());
        }
    }

}
