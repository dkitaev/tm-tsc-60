package ru.tsc.kitaev.tm.repository.model;

import lombok.AllArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.api.repository.model.IUserRepository;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.model.User;

import java.util.List;

@Repository
@AllArgsConstructor
public class UserRepository extends AbstractRepository implements IUserRepository {

    @Override
    public void add(@NotNull User user) {
        entityManager.persist(user);
    }

    @Override
    public void update(@NotNull User user) {
        entityManager.merge(user);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM User")
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return entityManager
                .createQuery("FROM User", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public User findById(@NotNull String id) {
        return entityManager
                .createQuery("FROM User u WHERE u.id = :id", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .getResultList()
                .stream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public User findByIndex(@NotNull Integer index) {
        return entityManager
                .createQuery("FROM User", User.class)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultList()
                .stream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager.remove(findById(id));
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("FROM User u WHERE u.login = :login", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .getResultList()
                .stream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull String email) {
        return entityManager
                .createQuery("FROM User u WHERE u.email = :email", User.class)
                .setParameter("email", email)
                .getResultList()
                .stream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void removeByLogin(@NotNull String login) {
        entityManager.remove(findByLogin(login));
    }

}
