package ru.tsc.kitaev.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kitaev.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.kitaev.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.kitaev.tm.api.service.ILoggerService;
import ru.tsc.kitaev.tm.api.service.dto.IProjectTaskDTOService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIndexException;
import ru.tsc.kitaev.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import java.util.List;

@Service
@AllArgsConstructor
public final class ProjectTaskDTOService extends AbstractDTOService implements IProjectTaskDTOService {

    @NotNull
    @Autowired
    public IProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    public ITaskDTORepository taskRepository;

    @NotNull
    @Override
    public List<TaskDTO> findTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    @Transactional
    public void bindTaskById(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
        if (taskRepository.findById(userId, taskId) == null) throw new TaskNotFoundException();
        @NotNull final TaskDTO task = taskRepository.findById(userId, taskId);
        task.setProjectId(projectId);
        taskRepository.update(task);
    }

    @Override
    @Transactional
    public void unbindTaskById(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
        if (taskRepository.findById(userId, taskId) == null) throw new TaskNotFoundException();
        @NotNull final TaskDTO task = taskRepository.findById(userId, taskId);
        task.setProjectId(null);
        taskRepository.update(task);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeById(userId, projectId);
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final String projectId = projectRepository.findByIndex(userId, index).getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeByIndex(userId, index);
    }

    @Override
    @Transactional
    public void removeByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final String projectId = projectRepository.findByName(userId, name).getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeByName(userId, name);
    }

}
