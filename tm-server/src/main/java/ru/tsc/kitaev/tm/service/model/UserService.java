package ru.tsc.kitaev.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kitaev.tm.api.repository.model.IUserRepository;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.api.service.model.IUserService;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.empty.*;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.exception.user.EmailExistsException;
import ru.tsc.kitaev.tm.exception.user.LoginExistsException;
import ru.tsc.kitaev.tm.exception.user.UserNotFoundException;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.util.HashUtil;

import java.util.List;

@Service
@AllArgsConstructor
public final class UserService extends AbstractService implements IUserService {

    @NotNull
    @Autowired
    public IUserRepository userRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @Transactional
    public void clear() {
        userRepository.clear();
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        return userRepository.findById(id);
    }

    @NotNull
    @Override
    public User findByIndex(@NotNull final Integer index) {
        if (index < 0) throw new EmptyIndexException();
        return userRepository.findByIndex(index);
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String id) {
        userRepository.removeById(id);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return userRepository.findById(id) != null;
    }

    @Override
    public boolean existsByIndex(final int index) {
        if (index < 0) throw new EmptyIndexException();
        userRepository.findByIndex(index);
        return true;
    }

    @Override
    @Transactional
    public void addAll(@NotNull final List<User> users) {
        for (User user : users) {
            userRepository.add(user);
        }
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login) == null;
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isEmailExists(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email) == null;
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password) {
        if (!isLoginExists(login)) throw new LoginExistsException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        setPassword(user, password);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (!isLoginExists(login)) throw new LoginExistsException();
        if (!isEmailExists(email)) throw new EmailExistsException();
        isEmailExists(email);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        setPassword(user, password);
        user.setEmail(email);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (!isLoginExists(login)) throw new LoginExistsException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        setPassword(user, password);
        user.setRole(role);
        userRepository.add(user);
        return user;
    }

    @Override
    @Transactional
    public void setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) throw new EntityNotFoundException();
        setPassword(user, password);
        userRepository.update(user);
    }

    @Override
    public void setPassword(@Nullable final User user, @Nullable final String password) {
        if (user == null) throw new EntityNotFoundException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(secret, iteration, password);
        user.setPasswordHash(hash);
    }

    @Override
    @Transactional
    public void updateUser(
            @Nullable final String userId,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final User user = findById(userId);
        if (user == null) throw new EntityNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        userRepository.update(user);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        userRepository.update(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        userRepository.update(user);
    }

}
